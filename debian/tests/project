#!/bin/sh

# Note:
#
# The debci infrastructure had problems with this test between March and June
# of 2016.  The gtml script would run without errors, but the Makefile would
# never be generated.  This didn't make much sense, because the build always
# worked in the official Reproducible Builds environment and also in all of my
# local test environments.  
#
# Starting with version 3.5.4-17, I added a lot of extra diagnostic information
# both in here and in the gtml script itself (via improve-error-handling.patch), 
# in an attempt to debug the problem.  In the end, there was still nothing
# useful -- no errors, but the file just wasn't created.  
#
# Clearly something is different in the real debci environment than in my own
# local debci test environment, but I have no idea what that is or how to fix
# it.  I wrote debian-qa and debian-perl for advice [1], but didn't get any
# responses.  So, I've reluctantly decided to just not run this test in any
# debci environment.  I detect the debci environment by looking for an
# executing user named debci.
#
# [1] https://lists.debian.org/debian-qa/2016/06/msg00023.html

# Make sure $AUTOPKGTEST_TMP is available
if [ "${AUTOPKGTEST_TMP}"F = "F" ]; then
   echo "Error: expected environment variable AUTOPKGTEST_TMP is not set."
   exit 1
fi

# Make sure $AUTOPKGTEST_ARTIFACTS is available
if [ "${AUTOPKGTEST_ARTIFACTS}"F = "F" ]; then
   echo "Error: expected environment variable AUTOPKGTEST_ARTIFACTS is not set."
   exit 1
fi

# Determine the test execution mode 
if [ "${1}"F = "F" ]; then
   MODE="autopkgtest"
elif [ "${1}" = "-r" ]; then
   MODE="debian/rules"
else 
   echo "usage: $0 [-r]\n-r Run in debian/rules mode instead of autopkgtest mode"
   exit 2
fi

# Determine locations of required tools
SOURCE_TREE="${PWD}"
if [ "${MODE}" = "debian/rules" ]; then
   # In debian/rules mode, the executables are assumed to be in the source tree
   GTML="${SOURCE_TREE}/gtml"
else
   # In autopkgtest mode, the executables are assumed to be installed
   GTML="/usr/bin/gtml"
fi

# Print a test summary
echo ""
echo "========================================================================="
echo "Running ${0} in mode: ${MODE}"
echo "========================================================================="
echo "SOURCE_TREE..........: ${SOURCE_TREE}"
echo "AUTOPKGTEST_TMP......: ${AUTOPKGTEST_TMP}"
echo "AUTOPKGTEST_ARTIFACTS: ${AUTOPKGTEST_ARTIFACTS}"
echo "GTML.................: ${GTML}"
echo "========================================================================="
echo ""

# Check whether we're in the debci environment and exit if we are (see notes above).
if [ `id -un` = 'debci' ]; then
   echo "Skipping tests in the debci environment."
   exit 0
fi 

# Always run tests from within $AUTOPKGTEST_TMP
cd ${AUTOPKGTEST_TMP}

# Set up some file and directory locations 
PROJECT="${AUTOPKGTEST_TMP}/project"
CONFIG="${AUTOPKGTEST_TMP}/config"
SOURCE="${AUTOPKGTEST_TMP}/source"
GTP="${PROJECT}/project.gtp"
MAKEFILE="${AUTOPKGTEST_TMP}/Makefile"

# Copy in the project directory
echo "Creating project in ${PROJECT}..."
mkdir -p ${PROJECT}
cp -r ${SOURCE_TREE}/debian/tests/data/project/config ${CONFIG}
cp -r ${SOURCE_TREE}/debian/tests/data/project/source ${SOURCE}

# Create the project.gtp file, which requires an absolute include path
echo "Creating GTP file: ${GTP}..."
rm -f ${GTP}
echo "define INCLUDE_PATH ${CONFIG}" >> ${GTP}
echo "allsource" >> ${GTP}

# Create the GTML makefile
echo "Creating Makefile: ${MAKEFILE}..."
${GTML} -M${MAKEFILE} ${GTP}
if [ $? != 0 ]; then
   echo "Error: failed to create GTML makefile."
   exit 1
elif [ ! -f ${MAKEFILE} ]; then
   echo "Error: GTML apparently did not generate Makefile"
   exit 1
fi

# Remove the GTML variable so we can use our own path later
sed -i '/GTML = gtml/d' ${MAKEFILE}

# Build the site
echo "Making the project...."
GTML=${GTML} /usr/bin/make
if [ $? != 0 ]; then
   echo "Make failed."
   exit 1
fi

# Check that every expected file exists and matches expectations
echo "Confirming expected results..."
for EXPECTED in ${SOURCE_TREE}/debian/tests/data/project/expected/*.html; do
   ACTUAL="${SOURCE}/`basename ${EXPECTED}`"
   echo "diff -Naur ${EXPECTED} ${ACTUAL}"
   /usr/bin/diff -Naur ${EXPECTED} ${ACTUAL}
   if [ $? != 0 ]; then
      echo "Generated result differs."
      exit 1
   fi
done
echo "Actual results match expected results... success!"

# Close the test
echo ""

